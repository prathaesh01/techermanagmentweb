

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.cj.protocol.Resultset;

/**
 * Servlet implementation class Edit
 */
@WebServlet("/Edit")
public class Edit extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Edit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/teacher","root","root");
			Statement statement = con.createStatement();

			System.out.println(request.getParameter("id"));
			int id = Integer.parseInt(request.getParameter("id"));
			
			PreparedStatement st = con.prepareStatement("update users set StaffID = ?,UserName = ?,Password = ?,Subject = ?,Standerd = ? where ID=?");
		
			st.setString(1, request.getParameter("StaffID"));
			st.setString(2, request.getParameter("UserName"));
			st.setString(3, request.getParameter("password"));
			st.setString(4, request.getParameter("Subject"));
			st.setString(5, request.getParameter("Standerd"));
			st.setInt(6, id);

			st.executeUpdate();
	        response.sendRedirect("Home.jsp");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	
	
	}

}
