

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jasper.tagplugins.jstl.core.Out;

import java.sql.*;

/**
 * Servlet implementation class TeacherData
 */
@WebServlet("/TeacherData")
public class TeacherData extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TeacherData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/teacher","root","root");
			Statement statement = con.createStatement();
			PreparedStatement st = con.prepareStatement("insert into users(StaffID,UserName,Password,Subject,Standerd) values(?,?,?,?,?)");

			st.setString(1, request.getParameter("StaffID"));
			st.setString(2, request.getParameter("UserName"));
			st.setString(3, request.getParameter("password"));
			st.setString(4, request.getParameter("Subject"));
			st.setString(5, request.getParameter("Standerd"));

			st.executeUpdate();
            PrintWriter out = response.getWriter();
            out.println("<html><body><b>Successfully Inserted"+ "</b></body></html>");
            response.sendRedirect("Home.jsp");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println();
		}  
		
	}

}
