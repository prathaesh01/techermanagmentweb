

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/LoginServ")
public class LoginServ extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
    public LoginServ() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");  
        PrintWriter out=response.getWriter();  
  		String UserName=request.getParameter("name");
  		String Password=request.getParameter("password");
  		if(Password.equals("Admin123")) {
  			HttpSession session = request.getSession();
  			session.setAttribute("username", UserName);
  			//RequestDispatcher rd=request.getRequestDispatcher("Home"); 
//  			System.out.println("called");
  			response.sendRedirect("Home.jsp");
  	        //rd.forward(request, response);  
  		}else {
  			RequestDispatcher rd=request.getRequestDispatcher("Error.jsp");  
  	        rd.include(request, response);  
  		}
	}

}
