<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div class="container col-md-5">
		<div class="card">
			<div class="card-body">
				<h2 class="text-center">Edit User Details</h2>
				<form action="EditStudent" method="post">

					<label><%=request.getParameter("id")%></label>
					<input type="hidden" name = "id" value = "<%=request.getParameter("id")%>">
					<fieldset class="form-group">
						<label>User Name</label> <input type="text" class="form-control"
							name="Name">
					</fieldset>

					<fieldset class="form-group">
						<label>Roll Number</label> <input type="text"
							class="form-control" name="RollNo">
					</fieldset>

					<fieldset class="form-group">
						<label>Age</label> <input type="text"
							class="form-control" name="Age">
					</fieldset>
					


					<button type="submit" class="btn btn-success ">Save</button>
				</form>
			</div>
		</div>
	</div>
</body>
</html>