<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
 <%@ page import="java.sql.*" %>

         <html>

        <head>
            <title>User Management Application</title>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        </head>

        <body>

        
            <br>

            <div class="row">
                <!-- <div class="alert alert-success" *ngIf='message'>{{message}}</div> -->

                <div class="container">
<div class="container text-right">
                            <a href="<%=request.getContextPath()%>/Logout" class="btn btn-success">Logout</a>
            </div>                                        <h2 class="container"><%=session.getAttribute("username") %></h2>
                
                    <h3 class="text-center">List of Users</h3>
                    <hr>
                    <div class="container text-left">

                        <a href="<%=request.getContextPath()%>/Form.jsp" class="btn btn-success">Add New User</a>
                    </div>
                    <br>
			<table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Password</th>
                                <th>Subject</th>
                                <th>StaffID</th>
                                <th>Standerd</th>

                            </tr>
                        </thead>
                        <tbody>
							<%
							 Class.forName("com.mysql.cj.jdbc.Driver");  
                            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/teacher","root","root");
                            Statement statement = con.createStatement();
                            ResultSet rs = statement.executeQuery("select * from users");

				             while (rs.next()) 
				             {  
				                 int ID = rs.getInt("ID");  
				                 String UserName = rs.getString("UserName");  
				                 String Password = rs.getString("password");  
				                 String Subject = rs.getString("Subject");   
				                 String StaffID = rs.getString("StaffID");   
				                 String Standerd = rs.getString("Standerd");
				                 %>	

				                <tr>
				                <td><%= rs.getInt("ID")  %></td>
				                <td><%= rs.getString("UserName") %></td>
				                <td><%= rs.getString("password") %></td>
				                <td><%= rs.getString("Subject") %></td>
				                <td><%= rs.getString("StaffID") %></td>
				                <td><%= rs.getString("Standerd") %></td>
				                <td><a href="<%=request.getContextPath()%>/EditData.jsp?id=<%=rs.getInt("ID")%>">Edit</a></td>
								<td><a href="<%=request.getContextPath()%>/Delete.jsp?id=<%=rs.getInt("ID")%>">Delete</a></td>
								</tr>
				           <% 
				             }  
				               %>
                        </tbody>

                    </table>
                </div>
            </div>
        </body>

        </html>