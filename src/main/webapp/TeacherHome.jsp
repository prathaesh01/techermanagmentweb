<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
 <%@ page import="java.sql.*" %>

         <html>

        <head>
            <title>User Management Application</title>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        </head>

        <body>

        
            <br>
            <div class="container text-right">
                            <a href="<%=request.getContextPath()%>/Logout" class="btn btn-success">Logout</a>
            </div>
                        <h2 class="container"><%=session.getAttribute("UserName") %></h2>
            

            <div class="row">
                <!-- <div class="alert alert-success" *ngIf='message'>{{message}}</div> -->

                <div class="container">
                
                    <h3 class="text-center">List of Students</h3>
                    <hr>
                    <div class="container text-left">

                        <a href="<%=request.getContextPath()%>/StudentForm.jsp" class="btn btn-success">Add New Student</a>
                    </div>
                    <br>
			<table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Roll Number</th>
                                <th>Age</th>
                                

                            </tr>
                        </thead>
                        <tbody>
							<%
							 Class.forName("com.mysql.cj.jdbc.Driver");  
                            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/teacher","root","root");
                            Statement statement = con.createStatement();

                            ResultSet rs = statement.executeQuery("select * from students");

				             while (rs.next()) 
				             {  
				                 int ID = rs.getInt("ID");  
				                 String UserName = rs.getString("Name");  
				                 int Password = rs.getInt("RollNo");  
				                 int Subject = rs.getInt("Age");   
				                 
				                 %>	

				                <tr>
				                <td><%= rs.getInt("ID")  %></td>
				                <td><%= rs.getString("Name") %></td>
				                <td><%= rs.getInt("RollNo") %></td>
				                <td><%= rs.getInt("Age") %></td>
				                <td><a href="<%=request.getContextPath()%>/EditStudentData.jsp?id=<%=rs.getInt("ID")%>">Edit</a></td>
								<td><a href="<%=request.getContextPath()%>/DeleteStudent.jsp?id=<%=rs.getInt("ID")%>">Delete</a></td>
								</tr>
				           <% 
				             }  
				               %>
                        </tbody>

                    </table>
                </div>
            </div>
        </body>

        </html>